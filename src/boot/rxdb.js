import Vue from 'vue'
import {
  createRxDatabase,
  RxDatabase,
  RxCollection,
  RxJsonSchema,
  RxDocument,
  isRxDatabase,
  addRxPlugin
} from "rxdb";
addRxPlugin(require("pouchdb-adapter-idb"));
addRxPlugin(require('pouchdb-adapter-http'));

// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({ app, router, store, Vue }) => {

  const db = await createRxDatabase({
    name: "heroesdb",
    adapter: "idb"
  }); // create database

  const mySchema = {
    keyCompression: true, // set this to true, to enable the keyCompression
    version: 0,
    title: "human schema no compression",
    type: "object",
    properties: {
      name: {
        type: "string",
        primary: true // <- this means: unique, required, string and will be used as '_id'
      }
    },
    required: ["name"]
  };
  await db.collection({ name: "heroes", schema: mySchema }); // create collection

  // replication
  const replicationState = db.heroes.sync({
      remote: 'http://admin:Liara2015@127.0.0.1:5984/syncer', // remote database. This can be the serverURL, another RxCollection or a PouchDB-instance
      waitForLeadership: true,              // (optional) [default=true] to save performance, the sync starts on leader-instance only
      direction: {                          // direction (optional) to specify sync-directions
          pull: true, // default=true
          push: true  // default=true
      },
      options: {                             // sync-options (optional) from https://pouchdb.com/api.html#replication
          live: true,
          retry: true
      },
      query: db.heroes.find() // query (optional) only documents that match that query will be synchronised
  });

  Vue.prototype.$db = db
}
